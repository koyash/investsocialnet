﻿using Dal.InterfaceRealization.Example;
using Dal.Interfaces.Example;

namespace Dal.Base
{
    public static class DalService
    {
        public static void Init()
        {
            Example = new Example();
        }
        
        public static IExample Example { get; set; }
        
    }
}