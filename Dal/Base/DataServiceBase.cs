﻿using System;
using System.Net.Http;
using Dal.Helpers;
using Refit;

namespace Dal.Base
{
    public class BaseDataService
    {
        // ReSharper disable once InconsistentNaming
        static readonly Lazy<HttpClient> _httpClient = new Lazy<HttpClient>(() => new HttpClient(TaskCanceledHandler)
        {
            BaseAddress = new Uri(AppSettings.SelectedUri),
        });

        protected static HttpClient HttpClient => _httpClient.Value;


        protected static readonly HttpLoggingHandler HttpLoggingHandler = new HttpLoggingHandler(CreateHandler());
        private static readonly TaskCanceledHandler TaskCanceledHandler = new TaskCanceledHandler();

        private static HttpClientHandler CreateHandler()
        {
            var handler = new HttpClientHandler();

            handler.ServerCertificateCustomValidationCallback +=
                (sender, certificate, chain, errors) => { return true; };

            return handler;
        }
    }


    public class BaseDataService<T> : BaseDataService
    {
        protected T InstanceInterface
        {
            get
            {
                var client = new HttpClient(HttpLoggingHandler)
                {
                    BaseAddress = new Uri(AppSettings.SelectedUri)
                };

                return RestService.For<T>(client);
            }
        }
    }
}