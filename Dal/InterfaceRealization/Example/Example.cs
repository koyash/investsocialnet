﻿using System.Reflection;
using System.Threading.Tasks;
using Dal.Base;
using Dal.Interfaces.Example;

namespace Dal.InterfaceRealization.Example
{
    public class Example : BaseDataService<IExample>, IExample
    {
        public async Task Login(object someObject)
        {
            await InstanceInterface.Login(new object());
            // return await InstanceInterface.Login(new object()); <- this is correct also Login should return something, but for example its just Task
        }
    }
}