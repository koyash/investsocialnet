﻿using System.Threading.Tasks;
using Refit;

namespace Dal.Interfaces.Example
{
    public interface IExample
    {
        [Post("/somePost")]
        Task Login([Body] object someObject);
    }
}