﻿using InvestSocialNet.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InvestSocialNet.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {}


        public DbSet<User> Users { get; set; }

        public DbSet<Token> Tokens { get; set; }

        public DbSet<Chat> Chats { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Comment> Comments { get; set; }
        
        public DbSet<News> News { get; set; }
    }
}