﻿using System;

namespace InvestSocialNet.Data.Dto
{
    public class ApiNews
    {
        public ApiNews(string title, string content, DateTime dateTime, string link)
        {
            Title = title;
            Content = content;
            CreationTime = dateTime;
            Link = link;
        }


        public string Title { get; set; }
        
        public string Content { get; set; }
        
        public DateTime CreationTime { get; set; }
        
        public string Link { get; set; }
    }
}