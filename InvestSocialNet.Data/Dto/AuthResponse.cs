﻿using System;

namespace InvestSocialNet.Data.Dto
{
    public class AuthResponse
    {
        public AuthResponse(Guid userId, string authToken)
        {
            UserId = userId;
            AuthToken = authToken;
        }

        public Guid UserId { get; set; }

        public string AuthToken { get; set; }
    }
}