﻿namespace InvestSocialNet.Data.Dto
{
    public class PostUser
    {       
        public PostUser(string firsName, string surname, string patronymic, bool anonymous, string username, string password)
        {
            FirsName = firsName;
            Surname = surname;
            Patronymic = patronymic;
            Anonymous = anonymous;
            Username = username;
            Password = password;
        }               

        public PostUser(string username, string password)
        {
            Password = password;
            Username = username;
        }


        public string FirsName { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public bool Anonymous { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}