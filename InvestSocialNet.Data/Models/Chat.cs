﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class Chat
    {
        public Chat(Guid sourceId)
        {
            TargetId = sourceId;
        }

        protected Chat()
        {}


        public Guid ChatId { get; protected set; }

        public Guid TargetId { get; protected set; }
    }
}