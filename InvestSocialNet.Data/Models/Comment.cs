﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class Comment
    {
        public Comment(Guid userId, string content, DateTime commentDate)
        {
            UserId = userId;
            Content = content;
            CommentDate = commentDate;
        }

        protected Comment()
        {}


        public Guid CommentId { get; protected set; }

        public Guid UserId { get; protected set; }

        public string Content { get; protected set; }

        public DateTime CommentDate { get; protected set; }
    }
}