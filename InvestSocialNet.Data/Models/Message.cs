﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class Message
    {
        public Message(Guid userId, Guid chatId, string content, DateTime messageDate)
        {
            UserId = userId;
            ChatId = chatId;
            Content = content;
            MessageDate = messageDate;
        }

        protected Message()
        {}


        public Guid MessageId { get; protected set; }

        public Guid UserId { get; protected set; }

        public Guid ChatId { get; protected set; }

        public string Content { get; protected set; }

        public DateTime MessageDate { get; protected set; }
    }
}