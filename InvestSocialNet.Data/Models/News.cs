﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class News
    {
        public News(int newsId, int userId, int postId, string title, string content, DateTime postDate, string newsPic)
        {
            NewsId = newsId;
            UserId = userId;
            PostId = postId;
            Title = title;
            Content = content;
            PostDate = postDate;
            NewsPic = newsPic;
        }


        public int NewsId { get; set; }

        public int UserId { get; set; }

        public int PostId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime PostDate { get; set; }

        public string NewsPic { get; set; }
    }
}