﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class Token
    {
        public Token(Guid userId, string value)
        {
            UserId = userId;
            Value = value;
        }
        
        public Guid TokenId { get; set; }

        public Guid UserId { get; set; }

        public string Value { get; set; }
    }
}