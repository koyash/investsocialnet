﻿using System;

namespace InvestSocialNet.Data.Models
{
    public class User
    {
        public User(string firsName, string surname, string patronymic, bool anonymous, string username, string password)
        {
            FirsName = firsName;
            Surname = surname;
            Patronymic = patronymic;
            Anonymous = anonymous;
            Username = username;
            Password = password;
        }

        protected User()
        {}


        public Guid UserId { get; protected set; }

        public string FirsName { get; protected set; }

        public string Surname { get; protected set; }

        public string Patronymic { get; protected set; }

        public bool Anonymous { get; protected set; }

        public string Username { get; protected set; }

        public string Password { get; protected set; }
    }
}