﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InvestSocialNet.Data;
using InvestSocialNet.Data.Dto;
using InvestSocialNet.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InvestSocialNet.WebHost.Controllers
{
    [ApiController]
    [Route("/news")]
    public class NewsController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;


        public NewsController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }


        [HttpPost]
        [Route("/postnews")]
        public Task PostNews(News news)
        {
            if (news == null)
            {
                throw new HttpListenerException(409);
            }

            try
            {
                _appDbContext.News.Add(news);
                _appDbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                throw new DbUpdateException();
            }

            return Task.CompletedTask;
        }
            
        [HttpGet]
        [Route("/getnews")]
        public List<News> GetNews()
        {
            return _appDbContext.News.ToList();
        }
        
        
        [HttpGet]
        [Route("/usernews")]
        public List<News> GetNewsWithUserId(PostNews news)
        {
            var currentNews = 
                _appDbContext.News.Where(s => s.UserId == news.UserId).ToList();
            if (currentNews == null)
            {
                throw new KeyNotFoundException();
            }

            return currentNews;
        }
        
        [HttpGet]
        [Route("/usernews")]
        public News GetNewsId(PostNews news)
        {
            var currentNews = 
                _appDbContext.News.FirstOrDefault(s => s.NewsId == news.NewsId);
            if (currentNews == null)
            {
                throw new KeyNotFoundException();
            }

            return currentNews;
        }
        
    }
}