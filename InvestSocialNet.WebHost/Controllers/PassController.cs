﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InvestSocialNet.Data;
using InvestSocialNet.Data.Dto;
using InvestSocialNet.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InvestSocialNet.WebHost.Controllers
{
    [ApiController]
    [Route("/pass")]
    public class PassController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private static readonly object LockRegisterUser = new();

        public PassController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }


        [HttpPost]
        [Route("/register")]
        public AuthResponse Register(PostUser user)
        {
            lock (LockRegisterUser)
            {
                var existedUser = _appDbContext.Users.FirstOrDefault(u => u.Username == user.Username);

                if (existedUser != null)
                    throw new HttpListenerException(409); // пока хз что получится

                var newUser = new User(user.FirsName, user.Surname, user.Patronymic, user.Anonymous, user.Username,
                    user.Password);

                _appDbContext.Users.Add(newUser);
                _appDbContext.SaveChanges();
                    
                var createdUser = _appDbContext.Users.First(u => u.Username == user.Username);
                
                var userToken = new Token(createdUser.UserId, $"{user.Username}.{user.Password}");

                _appDbContext.Tokens.Add(userToken);
                _appDbContext.SaveChanges();

                return new AuthResponse(createdUser.UserId, userToken.Value);
            }
        }

        [HttpPost]
        [Route("/authorize")]
        public async Task<AuthResponse> Authorize(string username, string password)
        {
            var user = await _appDbContext.Users.FirstOrDefaultAsync();

            if (user == null)
                throw new HttpListenerException(404);

            if (user.Password != password)
                throw new HttpListenerException(401);

            var token = await _appDbContext.Tokens.FirstOrDefaultAsync(t => t.UserId == user.UserId);
            return new AuthResponse(token.UserId, token.Value);
        }
    }
}