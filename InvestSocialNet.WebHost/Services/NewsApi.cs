﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using InvestSocialNet.Data.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace InvestSocialNet.WebHost.Services
{
    public class NewsApi
    {
        public static List<ApiNews> GetApiNews()
        {
            const string url = "https://newsapi.org/v2/everything?" +
                               "q=USD&" + // ключевое слово
                               "from=2021-11-27&" + // можно укатать DateTime.Now, параметром вчершего дня, для актуализации новостей
                               "sortBy=popularity&" +
                               "apiKey=42283139a32844659dd625265bc62c28";

            var json = new WebClient().DownloadString(url);
            var jsonSerialized = (JObject) JsonConvert.DeserializeObject(json);
            var articles = jsonSerialized["articles"].Children();

            var news = new List<ApiNews>();
            if (news == null) throw new ArgumentNullException(nameof(news));

            news.AddRange(articles
                .Select(value => new ApiNews(value["title"]
                    !.Value<string>(), value["content"]
                    !.Value<string>(), (DateTime) value["publishedAt"], value["url"]!.Value<string>())));

            return news;
        }
    }
}