﻿using Xamarin.Forms;

namespace InvestSocialNet.Base
{
    public class ContentPageBase : ContentPage
    {
        protected ViewModelBase ViewModelBase => BindingContext as ViewModelBase;
        protected ContentPageBase(ViewModelBase viewModel = null)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel;
        }
    }
    public class ContentPageBase<T> : ContentPageBase where T : ViewModelBase
    {
        public T ViewModel => ViewModelBase as T;

        protected ContentPageBase(T viewModelBase = null) : base(viewModelBase)
        {
            
        }

    }
}