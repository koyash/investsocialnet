﻿using Xamarin.Forms;

namespace InvestSocialNet.Base
{
    public class ContentViewBase : ContentView
    {
        protected ViewModelBase BaseViewModel => BindingContext as ViewModelBase;
        protected ContentViewBase(ViewModelBase viewModel = null)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel;
        }
    }

    public class ContentViewBase<T> : ContentViewBase where T : ViewModelBase
    {
        public T ViewModel => BaseViewModel as T;
        protected ContentViewBase(T viewModelBase = null) : base(viewModelBase)
        {
            
        }
    }
}