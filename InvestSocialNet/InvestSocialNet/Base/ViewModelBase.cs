﻿using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace InvestSocialNet.Base
{
    public class ViewModelBase
    {
        protected static Task<bool> NavigateTo(
            Page newPage,
            bool withAnimation = true)
        {
            var completedTask = new TaskCompletionSource<bool>();
            Device.BeginInvokeOnMainThread(async () =>
            {
                await GetMainNavigation().PushAsync(newPage, withAnimation);
            });
            return completedTask.Task;
        } 
        protected static Task<bool> ModalNavigateTo(
            Page newPage,
            bool withAnimation = true)
        {
            var completedTask = new TaskCompletionSource<bool>();
            Device.BeginInvokeOnMainThread(async () =>
            {
                await GetMainNavigation().PushModalAsync(newPage, withAnimation);
            });
            return completedTask.Task;
        }
        protected static Task<bool> RootNavigateTo(Page newPage)
        {
            var completedTask = new TaskCompletionSource<bool>();
            Device.BeginInvokeOnMainThread(async () =>
            {
                Application.Current.MainPage = new NavigationPage(newPage);
            });
            return completedTask.Task;
        }

        public Command GoBack => new Command(async () =>
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await GetMainNavigation().PopAsync();
            });
        });

        private static INavigation GetMainNavigation()
        {
            return Application.Current.MainPage.Navigation;
        }

        public static void ShowLoading()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading();
            });
        }

        public static void HideLoading()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }
    }
}