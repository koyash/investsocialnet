﻿using System;
using System.Globalization;
using InvestSocialNet.Modules.Chat;
using Xamarin.Forms;

namespace InvestSocialNet.Converters
{
    public class ChatMessagePosConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var message = (ChatMessageExample)value;
            if (message == null)
                return null;
            return message.FromId != -1 ? LayoutOptions.Start : LayoutOptions.End;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}