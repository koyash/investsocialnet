﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace InvestSocialNet.Custom
{
    public sealed class CustomProperty<T> : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private T _value;

        public T Value
        {
            set
            {
                _value = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Value"));
            }
            get => _value;
        }

        void OnTapped(object s)
        {
            
        }
    }
}