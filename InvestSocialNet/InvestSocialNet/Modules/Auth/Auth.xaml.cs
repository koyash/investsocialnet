﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Auth.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Auth : ContentPageBase<AuthViewModel>
    {
        public Auth() : base(new AuthViewModel())
        {
            InitializeComponent();
        }

        private void LoginClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(LoginEntry.Text)
                || string.IsNullOrEmpty(PassEntry.Text))
            {
                UserDialogs.Instance.Toast("Не все поля заполнены!", new TimeSpan(3));
                return;
            }

            ViewModel.LoginCommand.Execute(new LoginAndPassObject()
            {
                Login = LoginEntry.Text,
                Pass = PassEntry.Text
            });
        }
    }
}