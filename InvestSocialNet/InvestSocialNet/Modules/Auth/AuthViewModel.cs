﻿using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Auth.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Auth
{
    public class AuthViewModel : ViewModelBase
    {
        public ICommand LoginCommand => new Command<LoginAndPassObject>(async value =>
        {
            if (value.Login == "a" && value.Pass == "a")
            {
                Preferences.Set("token", "someToken");
                await RootNavigateTo(new Menu.Menu());
            }
        });

        public ICommand OpenRegistrationPage => new Command(async _ =>
        {
            await NavigateTo(new Registration.Registration());
        });
    }
}