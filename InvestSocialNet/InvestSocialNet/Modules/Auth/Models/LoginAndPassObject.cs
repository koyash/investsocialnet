﻿namespace InvestSocialNet.Modules.Auth.Models
{
    public class LoginAndPassObject
    {
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}