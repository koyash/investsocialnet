﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Chat
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Chat : ContentPageBase<ChatViewModel>
    {
        public Chat(int chatId, string fromName) : base(new ChatViewModel(chatId, fromName))
        {
            InitializeComponent();
        }
    }
}