﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Custom;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Chat
{
    public class ChatViewModel : ViewModelBase
    {
        private int ChatId;
        public string FromName { get; set; }
        public ObservableCollection<ChatMessageExample> Messages { get; set; }

        public ChatViewModel(int chatId, string fromName)
        {
            ChatId = chatId;
            FromName = fromName;
            Messages = new ObservableCollection<ChatMessageExample>();
            switch (chatId)
            {
                case 1:
                    Messages.Add(new ChatMessageExample()
                    {
                        FromId = 1,
                        Message = "Не хочешь заработать?",
                        Name = "Frederik"
                    });
                    break;
                case 2:
                    Messages.Add(new ChatMessageExample()
                    {
                        FromId = -1,
                        Message = "Я не знаю, что мне делать. Подскажи пожалуйста!",
                        Name = "Я"
                    });
                    Messages.Add(new ChatMessageExample()
                    {
                        FromId = 2,
                        Message = "Хочешь крутой совет?",
                        Name = "@superInvestor"
                    });
                    break;
                case 3:
                    Messages.Add(new ChatMessageExample()
                    {
                        FromId = -1,
                        Message = "Илон, привет! Во что нужно вкладыватсья?",
                        Name = "Я"
                    });
                    Messages.Add(new ChatMessageExample()
                    {
                        FromId = 3,
                        Message = "Надо было вложиться в биткон 10 лет назад",
                        Name = "Илон Маск"
                    });
                    break;
            }       
        }

        public ICommand SendMessageCommand => new Command(async  messageObject =>
        {
            var message = ((Entry)messageObject).Text;
            Messages.Add(new ChatMessageExample()
            {
                FromId = -1,
                Message = message,
                Name = "Я"
            });
            ((Entry)messageObject).Text = "";
        });

    }

    public class ChatMessageExample
    {
        public int FromId { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
    }
}