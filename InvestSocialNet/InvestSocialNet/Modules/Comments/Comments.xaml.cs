﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Cripta;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Comments
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Comments : ContentPageBase<CommentsViewModel>
    {
        public Comments(NewsCollection news) : base(new CommentsViewModel(news))
        {
            InitializeComponent();
        }

        public Comments(CriptaExample cripta) : base(new CommentsViewModel(cripta))
        {
            InitializeComponent();
        }
    }
}