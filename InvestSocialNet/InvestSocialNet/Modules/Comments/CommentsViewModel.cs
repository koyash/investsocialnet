﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Chat;
using InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Cripta;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Comments
{
    public class CommentsViewModel : ViewModelBase
    {
        
        public NewsCollection SelectedNews { get; set; }
        
        public ObservableCollection<CommentExample> Comments { get; set; }

        public CommentsViewModel()
        {
            
        }
        
        public CommentsViewModel(CriptaExample cripta)
        {
            SelectedNews = new NewsCollection()
            {
                Description = cripta.Diff,
                Title = cripta.Name,
                Author = cripta.Price,
                ImageSource = cripta.ImageName
            };
            Comments = new ObservableCollection<CommentExample>()
            {
                new CommentExample()
                {
                    Author = "Михаил Вячеславович",
                    Comment = "Я думаю, это хорошая новость!"
                },
                new CommentExample()
                {
                    Author = "@terminator3000",
                    Comment = "Интересно, как это повлияет на рынок?"
                }
            };
        }
        
        public CommentsViewModel(NewsCollection news)
        {
            SelectedNews = news;
            Comments = new ObservableCollection<CommentExample>()
            {
                new CommentExample()
                {
                    Author = "Михаил Вячеславович",
                    Comment = "Я думаю, это хорошая новость!"
                },
                new CommentExample()
                {
                    Author = "@terminator3000",
                    Comment = "Интересно, как это повлияет на рынок?"
                }
            };
        }
        public ICommand SendCommentCommand => new Command(async  messageObject =>
        {
            var message = ((Entry)messageObject).Text;
            Comments.Add(new CommentExample()
            {
                Author = "Я",
                Comment = message
            });
            ((Entry)messageObject).Text = "";
        });
    }

    public class CommentExample
    {
        public string Author { get; set; }
        public string Comment { get; set; }
    }
}