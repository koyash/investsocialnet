﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Init
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Init : ContentPageBase<InitViewModel>
    {
        public Init() : base(new InitViewModel())
        {
            InitializeComponent();
        }
    }
}