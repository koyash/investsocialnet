﻿using InvestSocialNet.Base;
using Xamarin.Essentials;

namespace InvestSocialNet.Modules.Init
{
    public class InitViewModel : ViewModelBase
    {
        public InitViewModel()
        {
            var token = Preferences.Get("token", null);
            if (string.IsNullOrEmpty(token))
                RootNavigateTo(new Auth.Auth());
            else
                RootNavigateTo(new Menu.Menu());
        }
    }
}