﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.Currencies
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Currency : ContentViewBase<CurrencyViewModel>
    {
        public Currency() : base(new CurrencyViewModel())
        {
            InitializeComponent();
        }
    }
}