﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Cripta
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cripta : ContentViewBase<CriptaViewModel>
    {
        public Cripta() : base(new CriptaViewModel())
        {
            InitializeComponent();
        }
    }
}