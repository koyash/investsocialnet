﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Cripta
{
    public class CriptaViewModel : ViewModelBase
    {
        
        public ObservableCollection<CriptaExample> Criptas { get; set; }

        public CriptaViewModel()
        {
            Criptas = new ObservableCollection<CriptaExample>()
            {
                new CriptaExample()
                {
                    Name = "Bitcoin",
                    ShortName = "BTC",
                    Price = "54349.8$",
                    Diff = "-287.17",
                    ImageName = "btc"
                },new CriptaExample()
                {
                    Name = "Etherium",
                    ShortName = "ETH",
                    Price = "4057.82$",
                    Diff = "-74.51",
                    ImageName = "eth"
                },new CriptaExample()
                {
                    Name = "Decentraland",
                    ShortName = "MANA",
                    Price = "4.522$",
                    Diff = "-0.139",
                    ImageName = "mana"
                },new CriptaExample()
                {
                    Name = "Tether",
                    ShortName = "USDT",
                    Price = "1.001$",
                    Diff = "0",
                    ImageName = "usdt"
                },new CriptaExample()
                {
                    Name = "Litecoin",
                    ShortName = "LTC",
                    Price = "188.98$",
                    Diff = "-7.22",
                    ImageName = "ltc"
                }
            };
        }
        public ICommand OpenCommentsCommand => new Command<CriptaExample>(async value =>
        {
            await NavigateTo(new Comments.Comments(value));
        });
    }

    public class CriptaExample
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Price { get; set; }
        public string Diff { get; set; }
        public string ImageName { get; set; }
        
    }
}