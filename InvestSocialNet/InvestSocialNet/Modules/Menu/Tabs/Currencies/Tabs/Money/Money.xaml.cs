﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Money
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Money : ContentViewBase<MoneyViewModel>
    {
        public Money() : base(new MoneyViewModel())
        {
            InitializeComponent();
        }
    }
}