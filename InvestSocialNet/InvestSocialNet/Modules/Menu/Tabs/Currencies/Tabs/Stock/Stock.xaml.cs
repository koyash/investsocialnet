﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.Currencies.Tabs.Stock
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Stock : ContentViewBase<StockViewModel>
    {
        public Stock() : base(new StockViewModel())
        {
            InitializeComponent();
        }
    }
}