﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.Messages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Messages : ContentViewBase<MessagesViewModel>
    {

        public Messages() : base(new MessagesViewModel())
        {
            InitializeComponent();
        }
    }

   
}