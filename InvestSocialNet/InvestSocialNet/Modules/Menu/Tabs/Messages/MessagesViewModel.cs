﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using InvestSocialNet.Base;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.Messages
{
    public class MessagesViewModel : ViewModelBase
    {
        
        public ObservableCollection<MessageModel> Messages { get; set; }

        public MessagesViewModel()
        {
            Messages = new ObservableCollection<MessageModel>()
            {
                new MessageModel()
                {
                    ChatId = 1,
                    FromName = "Frederik",
                    LastMessage = "Не хочешь заработать?"
                },
                new MessageModel()
                {
                    ChatId = 2,
                    FromName = "@superInvestor",
                    LastMessage = "Хочешь крутой совет?"
                },
                new MessageModel()
                {
                    ChatId = 3,
                    FromName = "Илон Маск",
                    LastMessage = "Надо было вложиться в биткон 10 лет назад"
                }
            };
        }

        public ICommand OpenChatCommand => new Command<MessageModel>(async value =>
        {
            await NavigateTo(new Chat.Chat(value.ChatId, value.FromName));
        });

    }
    public class MessageModel
    {
        public string FromName { get; set; }
        public string LastMessage { get; set; }
        public int ChatId { get; set; }
    }
}