﻿using InvestSocialNet.Base;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.News
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class News : ContentViewBase<NewsViewModel>
    {
        public News() : base(new NewsViewModel())
        {
            InitializeComponent();
        }
    }
}