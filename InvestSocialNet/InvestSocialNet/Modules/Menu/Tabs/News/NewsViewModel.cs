﻿using InvestSocialNet.Base;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.News
{
    
    public class NewsViewModel : ViewModelBase
    {
        public int SelectedViewModelIndex { get; set; } = 0;

        public int DeviceAvailableHeightForNews { get; set; } 

        public NewsViewModel()
        {
            DeviceAvailableHeightForNews = (int) DeviceDisplay.MainDisplayInfo.Height;
        }
    }
}