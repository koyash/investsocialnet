﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvestSocialNet.Base;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.News.Tabs.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Common : ContentViewBase<CommonViewModel>
    {
        public Common() : base(new CommonViewModel())
        {
            InitializeComponent();
        }

        private async void OpenBrowserTapped(object sender, EventArgs e)
        {
            var url = ((Button)sender).BindingContext as string;

            await Browser.OpenAsync(url);
        }
    }
}