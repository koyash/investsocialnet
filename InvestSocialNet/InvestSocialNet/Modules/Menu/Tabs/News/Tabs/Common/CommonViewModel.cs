﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.News.Tabs.Common
{
    public class CommonViewModel : ViewModelBase
    {
        public ObservableCollection<NewsCollection> NewsData { get; set; }
        private List<NewsModel> TestData;
        private bool _firstDownload;

        public CommonViewModel()
        {
            TestData = new List<NewsModel>()
            {
                new NewsModel()
                {
                    Author = "facebook.com/some_url",
                    DateTime = new DateTime(2021, 11, 27),
                    Description = "Какая-то крутая новость про инвестии с сайта facebook или хз",
                    Title = "Какое-то название новости"
                },
                new NewsModel()
                {
                    Author = "somesite.com/some_url",
                    DateTime = new DateTime(2021, 11, 26),
                    Description = "Супер крутая новость, но не такая как последняя",
                    Title = "Внимание, новость!",
                    ImageUrl =
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png"
                }
            };
            NewsData = new ObservableCollection<NewsCollection>();
            DownloadData();

        }

        public async Task DownloadData()
        {
            if (_firstDownload)
                return;
            _firstDownload = true;
            foreach (var testData in TestData)
            {
                string filePath = "";
                if (!string.IsNullOrEmpty(testData.ImageUrl))
                {
                    filePath = Path.Combine(FileSystem.AppDataDirectory, Path.GetFileName(testData.ImageUrl));
                    if (!File.Exists(filePath))
                    {
                        var bytes = await DownloadFile(testData.ImageUrl, filePath);
                        File.WriteAllBytes(filePath, bytes);
                    }
                }
                NewsData.Add(new NewsCollection()
                {
                    Title = testData.Title,
                    Author = testData.Author,
                    Description = testData.Description,
                    DateTime = testData.DateTime,
                    ImageSource = new FileImageSource(){File = filePath}
                });
            }
        }
        
        private async Task<byte[]> DownloadFile(string imageUrl, string filePath)
        {
            var httpClient = new HttpClient { Timeout = TimeSpan.FromSeconds(5) };
            try
            {
                using (var httpResponse = await httpClient.GetAsync(imageUrl))
                {
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return await httpResponse.Content.ReadAsByteArrayAsync();
                    }

                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ICommand OpenBrowserCommand => new Command<string>(async value =>
        {
            if (string.IsNullOrEmpty(value))
                return;
            try
            {
                await Browser.OpenAsync(new Uri(value), BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Ошибка при переходе по ссылке. Возможно ссылка не верна!", new TimeSpan(5));
            }
        });

        public ICommand OpenCommentsCommand => new Command<NewsCollection>(async value =>
        {
            await NavigateTo(new Comments.Comments(value));
        });
    }
}