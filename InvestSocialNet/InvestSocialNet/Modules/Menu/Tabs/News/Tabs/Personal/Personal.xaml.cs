﻿using InvestSocialNet.Base;
using Xamarin.Forms.Xaml;

namespace InvestSocialNet.Modules.Menu.Tabs.News.Tabs.Personal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Personal : ContentViewBase<PersonalViewModel>
    {
        public Personal() : base(new PersonalViewModel())
        {
            InitializeComponent();
        }
    }
}