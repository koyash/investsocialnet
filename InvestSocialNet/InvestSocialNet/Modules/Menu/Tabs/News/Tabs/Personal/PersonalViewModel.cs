﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.News.Tabs.Personal
{
    public class PersonalViewModel : ViewModelBase
    {
        public ObservableCollection<NewsCollection> NewsData { get; set; }
        private List<NewsModel> TestData;
        private bool _firstDownload;

        public PersonalViewModel()
        {
            TestData = new List<NewsModel>()
            {
                new NewsModel()
                {
                    Author = "@habrCreator",
                    DateTime = new DateTime(2021, 11, 27),
                    Description = "Это сайт на котором выкладываются крутые статьи из мира ит.",
                    Title = "Про habr."
                },
                new NewsModel()
                {
                    Author = "@superInvestor",
                    DateTime = new DateTime(2021, 11, 26),
                    Description = "Подписывайтесь на меня! ",
                    Title = "Я выводу вас на новый уровень!",
                    ImageUrl ="https://www.banki.ru/static/bundles/ui-2018/8e42b165ee30fb9369af.png"
                }
            };
            NewsData = new ObservableCollection<NewsCollection>();
            DownloadData();

        }

        public async Task DownloadData()
        {
            if (_firstDownload)
                return;
            _firstDownload = true;
            foreach (var testData in TestData)
            {
                string filePath = "";
                if (!string.IsNullOrEmpty(testData.ImageUrl))
                {
                    filePath = Path.Combine(FileSystem.AppDataDirectory, Path.GetFileName(testData.ImageUrl));
                    if (!File.Exists(filePath))
                    {
                        var bytes = await DownloadFile(testData.ImageUrl, filePath);
                        File.WriteAllBytes(filePath, bytes);
                    }
                }
                NewsData.Add(new NewsCollection()
                {
                    Title = testData.Title,
                    Author = testData.Author,
                    Description = testData.Description,
                    DateTime = testData.DateTime,
                    ImageSource = new FileImageSource(){File = filePath}
                });
            }
        }
        
        private async Task<byte[]> DownloadFile(string imageUrl, string filePath)
        {
            var httpClient = new HttpClient { Timeout = TimeSpan.FromSeconds(5) };
            try
            {
                using var httpResponse = await httpClient.GetAsync(imageUrl);
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    return await httpResponse.Content.ReadAsByteArrayAsync();
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public ICommand OpenCommentsCommand => new Command<NewsCollection>(async value =>
        {
            await NavigateTo(new Comments.Comments(value));
        });
    }
}