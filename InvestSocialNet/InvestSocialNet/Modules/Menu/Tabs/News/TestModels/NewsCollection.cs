﻿using Xamarin.Forms;

namespace InvestSocialNet.Modules.Menu.Tabs.News.TestModels
{
    public class NewsCollection : NewsModel
    {
        public bool IsLoading { get; set; }
        
        public ImageSource ImageSource { get; set; }
    }
}