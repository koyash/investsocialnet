﻿using System;

namespace InvestSocialNet.Modules.Menu.Tabs.News.TestModels
{
    public class NewsModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; set; }
        public string Author { get; set; }
        public string ImageUrl { get; set; }
    }
}