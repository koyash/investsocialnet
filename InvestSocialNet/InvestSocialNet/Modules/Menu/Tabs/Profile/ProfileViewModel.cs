﻿using System;
using System.Collections.ObjectModel;
using InvestSocialNet.Base;
using InvestSocialNet.Modules.Menu.Tabs.News.TestModels;

namespace InvestSocialNet.Modules.Menu.Tabs.Profile
{
    public class ProfileViewModel : ViewModelBase
    {
        public ObservableCollection<NewsCollection> Publications { get; set; }

        public ProfileViewModel()
        {
            Publications = new ObservableCollection<NewsCollection>()
            {
                new NewsCollection()
                {
                    Description = "Это моя вторая публикация где есть много букаааааававалвпыла ыщавшпшщыв ашщопывашщпо щывашп",
                    Title = "Вторая публикация",
                    DateTime = new DateTime(2021, 11, 11)
                },
                new NewsCollection()
                {
                    Description = "Я попытался написать какую-то публикацию!)!)!)",
                    DateTime = new DateTime(2021, 10, 10),
                    Title = "Первая публикация"
                }
            };
        }
    }

}