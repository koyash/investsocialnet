﻿namespace InvestSocialNet.Modules.Registration.Models
{
    public class RegistrationObject
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}