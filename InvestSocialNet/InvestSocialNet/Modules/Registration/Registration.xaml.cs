﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Acr.UserDialogs.Infrastructure;
using InvestSocialNet.Base;
using InvestSocialNet.Custom;
using InvestSocialNet.Modules.Registration.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Log = Xamarin.Forms.Internals.Log;

namespace InvestSocialNet.Modules.Registration
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registration : ContentPageBase<RegistrationViewModel>
    {
        public Registration() : base(new RegistrationViewModel())
        {
            InitializeComponent();
        }

        private void IsAnonymous_OnCheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (sender == null)
                return;
            ViewModel.IsAnonymousProperty.Value = !((CheckBox)sender).IsChecked;
        }

        private void Registration_Clicked(object sender, EventArgs e)
        {
            // ReSharper disable once ReplaceWithSingleAssignment.False
            bool hasError = false;
            
            if (string.IsNullOrEmpty(LoginEntry.Text))
            {
                hasError = true;
                LoginEntry.PlaceholderColor = Color.Red;
                LoginEntry.Placeholder = "Это поле обязательно";
            }
            if (string.IsNullOrEmpty(PassEntry.Text))
            {
                hasError = true;
                PassEntry.PlaceholderColor = Color.Red;
                PassEntry.Placeholder = "Это поле обязательно";
            }
            if (hasError)
            {
                UserDialogs.Instance.Toast("Не все поля заполнены!", new TimeSpan(3));
                return;
            }
            
            ViewModel.RegistrationCommand.Execute(new RegistrationObject()
            {
                Name = NameEntry.Text,
                Surname = SurnameEntry.Text,
                Lastname = LastnameEntry.Text,
                Password = PassEntry.Text,
                Login = LoginEntry.Text
            });
        }
    }
}