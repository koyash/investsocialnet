﻿using System.Windows.Input;
using InvestSocialNet.Base;
using InvestSocialNet.Custom;
using InvestSocialNet.Modules.Registration.Models;
using Xamarin.Forms;

namespace InvestSocialNet.Modules.Registration
{
    public class RegistrationViewModel : ViewModelBase
    {
        public CustomProperty<bool> IsAnonymousProperty { get; set; } = new CustomProperty<bool>();

        public RegistrationViewModel()
        {
            IsAnonymousProperty.Value = true;
        }

        public ICommand RegistrationCommand = new Command<RegistrationObject>(value =>
        {
            
        });
    }
}